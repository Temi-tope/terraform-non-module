resource "aws_route53_record" "www" {
  zone_id = "Z0088821186BPEW1C8NKQ"
  name    =  "temi.com" // var.domain_name
  type    = "A"
  alias {
    name                   = "d1bp3mhzctbrb7.cloudfront.net"
// module.cloudfront.cloudfront_domain_name
    zone_id                = "Z2FDTNDATAQYW2"// module.cloudfront.cloudfront_zone_id
    evaluate_target_health = false
  }
}
// Z0088821186BPEW1C8NKQ (hosted zone id)