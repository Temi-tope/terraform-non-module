variable domain_name {
    description = "s3 web hosting url"
    type = string
}
variable origin_id {
    description = "s3-url"
    type = string
}