resource "aws_s3_bucket" "website_bucket" {
  bucket = var.bucket_name
  tags = {
    Name        = "Website-bucket"
  }
}
resource "aws_s3_bucket_website_configuration" "website_config" {
  bucket = aws_s3_bucket.website_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_public_access_block" "public_access" {
  bucket = aws_s3_bucket.website_bucket.id

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = false
  restrict_public_buckets = false
}

# module "template_files" {
#   source = "hashicorp/dir/template"
#   base_dir = "${path.module}/webpage"
# }

resource "aws_s3_bucket_policy" "public_policy" {
  bucket = aws_s3_bucket.website_bucket.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid       = "PublicReadGetObject",
        Effect    = "Allow",
        Principal = "*",
        Action    = "s3:GetObject",
        Resource  = "arn:aws:s3:::temitope-web-bucket/*"
      }
    ]
  })

}



# resource "aws_s3_object" "hosting_bucket_files" {
#   bucket = aws_s3_bucket.website_bucket.id

#   for_each = module.template_files.files

#   key = each.key
#   content_type = each.value.content_type

#   source = each.value.source_path
#   content = each.value.content

#   etag = each.value.digests.md
# }