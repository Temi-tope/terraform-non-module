module "s3_bucket" {
source = "./modules/s3"
bucket_name = var.bucket_name
}

locals {
  files_to_upload = fileset("${path.module}/webpage", "**/*")
}

resource "aws_s3_object" "files" {
  for_each = local.files_to_upload

  bucket = module.s3_bucket.bucket_name
  key    = each.value
  source = "${path.module}/webpage/${each.value}"
  content_type = each.value
  # content_type = lookup({
  #   "html" = "text/html",
  #   "css"  = "text/css",
  #   "js"   = "application/javascript",
  #   "png"  = "image/png",
  #   "jpg"  = "image/jpeg",
  #   "gif"  = "image/gif"
  # }, split(".", each.value)[length(split(".", each.value)) - 1], "application/octet-stream")

 // content_type = lookup(var.content_type_map, split(".", each.value)[1], "application/octet-stream")
}

 module "cloudfront" {
   source         = "./modules/cloudfront"
   domain_name    = var.domain_name
   // bucket_name    = module.s3_bucket.bucket_name
   origin_id      = var.origin_id
 // certificate_arn = var.certificate_arn
}
