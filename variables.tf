variable region {
    description = "Default region"
    type = string 
    default = "us-east-1"
}
variable "bucket_name" {
    description = "website-bucket"
    type = string 
}
variable "domain_name" {
    description = "s3 webhosting url"
    type = string
}
variable origin_id {
    description = "s3-url"
    type = string
}
# variable "content_type_map" {
#   description = "A map of file extensions to content types"
#   type        = map(string)
#   default = {
#     "html" = "text/html"
#     "css"  = "text/css"
#     "js"   = "application/javascript"
#     "png"  = "image/png"
#     "jpg"  = "image/jpeg"
#     "jpeg" = "image/jpeg"
#     "gif"  = "image/gif"
#   }
# }