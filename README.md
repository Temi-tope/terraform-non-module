### A brief description and instructions on how to use the project.

# Static Website Deployment on AWS using Terraform

This project sets up a static website hosted on AWS using Terraform module. It includes S3 for storage, CloudFront for content delivery, and Route 53 for DNS management.

## Prerequisites

- AWS Account
- Domain name registered in Route 53
- Terraform installed
- AWS CLI configured

## Setup

1. #### Configure your AWS credentials.
Ensure your AWS credentials are configured. You can use the AWS CLI to configure them:
 ``` 
 aws configure 
 ```
1. #### Update the variables in `variables.tf`:
Open the `variables.tf` file and set your domain name and other necessary variables.
3. #### Initialize the project:
Run `terraform init` to initialize the project.
4. #### Deploy the infrastructure:
Run `terraform apply` to deploy the infrastructure.

## File Structure
- modules : Contains the reusable modules for S3 and CloudFront.
- `api_gateway_config.tf`: Configures the API Gateway.
- `api_gateway_resources.tf`: Defines the API Gateway resources.
- `certificate.tf`: Creates the SSL certificate.
- `data.tf`: Fetches existing resources.
- `init.tf`: Initializes the provider and backend.
- `main.tf`: Main Terraform configuration.
- `outputs.tf`: Outputs useful information.
- `permissions.tf`: Handles IAM roles and policies.
- `route53.tf`: Configures Route 53 for DNS management.
- `variables.tf`: Defines variables.


#### Creating a bucket to store the website's static assets
We’ll start by creating an S3 bucket to store our website’s static assets. S3 provides highly scalable storage for hosting static files, and it integrates seamlessly with other AWS services. With Terraform, we define the S3 bucket's configuration such as its name, region, and permissions, in a declarative manner through a modularize terraform structure that contains a `variable.tf` and `main.tf`and `output.tf`.
![s3](./Screenshot%202024-06-28%20215940.png)


#### Content Delivery with AWS CloudFront
To distribute our website’s content efficiently, we’ll leverage AWS CloudFront, a globally distributed content delivery network. CloudFront caches our website’s files in edge locations worldwide, reducing latency and improving the overall performance for visitors across the globe. With Terraform, we configure CloudFront to serve our S3 bucket’s content through a modularize terraform structure similar to that of S3 that I stated earlier and take advantage of its advanced caching.
![cdn](./Screenshot%202024-06-28%20214233.png)

#### DNS Management with AWS Route 53
To connect our custom domain name to our CloudFront distribution, we’ll use AWS Route 53, a highly scalable and reliable domain name system (DNS) service. Route 53 enables us to manage DNS records and route traffic to our CloudFront distribution seamlessly. By configuring the appropriate DNS settings, we can ensure that our website is accessible using our custom domain name.

---
In the `route53.tf` configuration, it creates a DNS record in Amazon Route 53 for a domain and points it to a CloudFront distribution using an alias record 
![Route53](./Screenshot%202024-06-28%20172902.png)

#### variables.tf
This file defines the variables used throughout the Terraform configuration. Update this file with your specific details, such as your domain name.

#### main.tf
The main Terraform configuration file that orchestrates the creation of S3 buckets, CloudFront distributions, and one other resource which uploads S3 object that contains files that would be used to render our static web page.

#### certificate.tf
Creates an SSL certificate for the domain using AWS Certificate Manager (ACM) but in this case, we would use the default provided by CloudFront ` cloudfront_default_certificate = true `


#### outputs.tf
Defines the outputs for the Terraform configuration, which provides useful information such as the CloudFront distribution domain name and S3 bucket names.

data.tf
Fetches existing resources like Route 53 hosted zones and ACM certificates.
This is S3 website url 
![s3-url](./Screenshot%202024-06-28%20183335.png)
---
This is Cloudfront serving the webpage
![webpage](./Screenshot%202024-06-29%20082003.png)